
function myFuntion() {
    console.log("my funtion");
}
myFuntion()
//  FUNCION ANONIMA
let mi_funcion = function () {
    console.log("mi función");
}
mi_funcion
// ARROW FUNCION
let otra_funcion = () => console.log("otra funcion");
otra_funcion();

// *********************************************************************************

let main = function(){
    captura_boton();
}

let captura_boton = function(){
    document.querySelector(".myButton input").setAttribute("onclick","dataRead()");
}

let dataRead = function(){
    console.log("Intentaremos leer los datos del formulario");
    console.log(
        document.querySelector("#nombre").value,
        document.querySelector("#apellido").value,
        document.querySelector("#email").value,
        document.querySelector("#pass").value,          
    );
    let myObjeto ={
       /* //clave: valor */
       nombre : document.querySelector("#nombre").value,
       apellido : document.querySelector("#apellido").value,
       email : document.querySelector("#email").value,
       pass : document.querySelector("#pass").value, 
    };
    console.log(myObjeto);
     //json necesita pasar el objeto a string
    console.log(JSON.stringify(myObjeto));// ME SIRVIO PARA VER LO QUE HACE EL STRINGIFLY
    // Guardamos los datos en local storage (PERSISTENCIA DE DATOS)
    save_localStorage(myObjeto)

}
// con esto grabamos en localStorage
let save_localStorage = function(myObj){  
    localStorage.setItem("miInfo",JSON.stringify(myObj));

}
let read_localStorage = function(){
    let miInfo = localStorage.getItem("miInfo");
    console.log(miInfo);
    let myObjeto = JSON.parse(miInfo);
    console.log(myObjeto);
    document.querySelector("#nombre").value = myObjeto.nombre;
    document.querySelector("#apellido").value = myObjeto.apellido;
    document.querySelector("#email").value = myObjeto.email;
    document.querySelector("#pass").value = myObjeto.pass;
}
let reset = function(){
    document.querySelector("#nombre").value = "";
    document.querySelector("#apellido").value = "";
    document.querySelector("#email").value = "";
    document.querySelector("#pass").value = "";
}

main();